import React from 'react';
import { mount } from 'react-mounter';

// Local imports
import { MainLayout } from '/client/layouts/MainLayout.jsx';
import Navbar from '/client/components/default/Navbar.jsx';
import SimpleNavbar from '/client/components/default/SimpleNavbar.jsx';
import Footer from '/client/components/default/Footer.jsx';

// Containers
import User from '/client/containers/UserContainer.jsx';
import Transaction from '/client/containers/TransactionContainer.jsx';
import Home from '/client/components/home/Home.jsx';

// Defining routes
FlowRouter.route('/', {
	name: 'home',

	action() {
		mount(MainLayout, {
			navbar: <Navbar/>,
			content: <Home/>,
			footer: ''
		});
	}
});

FlowRouter.route('/profile', {
	name: 'profile',

	action(params) {

		mount(MainLayout, {
			navbar: <SimpleNavbar/>,
			content: <User/>,
			footer: <Footer/>
		});
	}
});

FlowRouter.route('/t/:id', {
	name: 'transaction',

	action(params) {

		mount(MainLayout, {
			navbar: <SimpleNavbar/>,
			content: <Transaction id={params.id}/>
		});
	}
});

FlowRouter.route('/complete', {
	name: 'complete',

	action(params) {
		window.location.href = 'https://s3-sa-east-1.amazonaws.com/masterlike/index.html';
	}
})