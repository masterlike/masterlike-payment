import React, { Component } from 'react';
import { Blaze } from 'meteor/blaze';
import ReactDOM from 'react-dom';


export default class LoginForm extends Component {
	componentDidMount() {
		this.view = Blaze.render(
			Template.loginButtons,
			ReactDOM.findDOMNode(this.refs.loginContainer)
		);
	}

	componentWillUnmount() {
		Blaze.remove(this.view);
	}
	
	render() {
		return (
			<div ref="loginContainer"></div>
		);
	}
};