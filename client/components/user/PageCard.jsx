import React, { Component } from 'react';

export default class PageCard extends Component {
	constructor() {
		super();

		this.showModal = this.showModal.bind(this);
	}

	showModal() {
		Session.set('modalData', this.props.page);
		Session.set('showModal', true);
	}

	formatNumber(num) {
		var n = num.toString();
		var p = n.indexOf('.');

		return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
			return p < 0 || i < p ? ($0+'.') : $0;
		});
	}

	render() {
		let page = this.props.page;

		return (
			<div className="c-card" onClick={this.showModal}>
				<img src={page.image} alt="" className="c-card__image"/>
				<div className="c-card__info">
					<h3 className="c-card__name">{page.name}</h3>
					<p className="c-card__sponsors">{this.formatNumber(page.sponsors)} seguidores</p>
				</div>
			</div>
		)
	}
};