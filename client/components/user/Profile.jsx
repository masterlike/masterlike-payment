import React, { Component } from 'react';
import PageCard from './PageCard.jsx';
import Modal from '../default/Modal.jsx';
import { pages } from '/lib/pages.js';

export default class Profile extends Component {
	constructor() {
		super();
	}

	render() {
		Session.set('loggedIn', Meteor.userId() !== null);

		let profile = this.props.userInfo;
		let url = typeof profile !== 'undefined'
			? 'http://graph.facebook.com/' + profile.user_id + '/picture?type=large'
			: '';

		let modal = Session.get('showModal') === true
			? <Modal/>
			: '';

		return (
			<section id="profile">
				<div className="c-profile">
					<div className="c-profile__avatar">
						<img src={url} alt=""/>
					</div>
					<div className="c-profile__info">
						<p className="c-profile__greeting">Hi</p>
						<h2 className="c-profile__name">{profile.first_name} {profile.last_name}</h2>
					</div>
				</div>
				<ul className="pageList">
					{pages.map((page, i) => (
						<li className="pageList__item" key={'page-' + i}>
							<PageCard page={page}/>
						</li>
					))}
				</ul>
				{modal}
			</section>
		)
	}
};