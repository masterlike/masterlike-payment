import React from 'react';

import LoginForm from '../login/LoginForm.jsx';

export default Hero = () => (
	<div className="c-hero">
		<div className="c-hero__mask"></div>
		<div className="c-hero__desc">
			<p>Você ainda não possui uma conta no MasterLike.</p>
			<p>Para prosseguir com seu patrocínio, entre com seu Facebook.</p>
			<LoginForm/>
		</div>
	</div>
);
