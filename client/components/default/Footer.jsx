import React from 'react';

export default Footer = () => (
	<footer className="footer">
		<h5> Feito a base chocolates, pizza &amp; drinks no <a class="white-text" href="http://mastercardshift.com/" target="_blank">SHIFT HACKATHON</a></h5>
	</footer>
);