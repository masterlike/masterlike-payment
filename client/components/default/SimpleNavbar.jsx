import React, { Component } from 'react';

export default class SimpleNavbar extends Component {
	constructor(props) {
		super();
	}

	render() {
		return (
			<header className="c-header c-header--simple" ref="sticky">
				<a href="/">
					<h3 className="c-header__brand" title="Master Like">MasterLike</h3>
				</a>
			</header>
		)
	}
}