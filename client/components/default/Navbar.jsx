import React, { Component } from 'react';

// Local imports
import LoginForm from '../login/LoginForm.jsx';


export default class Navbar extends Component {
	constructor(props) {
		super();
		
		this.scrollCallback = this.scrollCallback.bind(this);
	}

	componentDidMount() {
		window.onscroll = this.scrollCallback;
	}

	scrollCallback(e) {
		let sticky = this.refs.sticky;

		if (window.scrollY > 0)
			sticky.classList.add('is-fixed');
		else
			sticky.classList.remove('is-fixed');
	}

	render() {
		return (
			<header className="c-header" ref="sticky">
				<a href="/">
					<h3 className="c-header__brand" title="Master Like">MasterLike</h3>
				</a>
			</header>
		)
	}
}