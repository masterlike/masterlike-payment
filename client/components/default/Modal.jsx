import React, { Component } from 'react';
import PageCard from '../user/PageCard';
import $script from 'scriptjs';

export default class Profile extends Component {
	constructor() {
		super();

		this.state = { amount: 5 };
		this.updateAmount = this.updateAmount.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.sendPayment = this.sendPayment.bind(this);
	}

	updateAmount() {
		let amount = this.refs.amount.value;

		this.setState({ amount });
		this.forceUpdate();
	}

	closeModal() {
		Session.set('showModal', false);
	}

	sendPayment() {
		let btn = this.refs.button;

		$script('https://www.simplify.com/commerce/simplify.pay.js', function () {
			btn.click();
		});
	}

	render() {
		let data = Session.get('modalData');

		console.log('update', this.state.amount)
		return (
			<div className="c-modal">
				<div className="c-modal__mask" onClick={this.closeModal}></div>
				<div className="c-modal__container">
					<div className="c-modal__card">
						<PageCard page={data}/>
					</div>
					<div className="c-modal__content">
						<h3 className="c-modal__title">Detatlhes da página</h3>
						<div className="c-modal__group">
							<label className="c-modal__label">Descrição:</label>
							<span className="c-modal__value">{data.description}</span>
						</div>
						<div className="c-modal__group">
							<label className="c-modal__label">Email:</label>
							<span className="c-modal__value">teste@masterlike.com</span>
						</div>
						<div className="c-modal__group">
							<label className="c-modal__label">Websie:</label>
							<span className="c-modal__value">www.masterlike.com</span>
						</div>
						<div className="c-modal__bottom">
							<label className="c-modal__label c-modal__label--large">Valor</label>
							<input
								type="range"
								min="5"
								step="5"
								max="100"
								value={this.state.amount}
								className="c-modal__range"
								ref="amount"
								onChange={this.updateAmount}
							/>
							<span className="c-modal__indicator">R$ {this.state.amount}</span>
						</div>
						<button
							className="c-modal__button"
							data-sc-key="sbpb_ZDg2NWM5MGItMzFkZi00MDI4LTlmM2UtYWFkMzkwNzYxYjJi"
							data-name={data.name}
							data-description={'Donation para ' + data.name}
							data-amount={this.state.amount * 100}
							data-color="#12B830"
							data-currency="USD"
							data-receipt="true"
							data-masterpass="true"
							type="button"
							ref="button"
							onClick={this.sendPayment}
						>
							Finalizar
						</button>
					</div>
				</div>
			</div>
		)
	}
};
