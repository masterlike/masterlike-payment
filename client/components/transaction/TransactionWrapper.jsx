import React, { Component } from 'react';
import LoginForm from '../login/LoginForm.jsx';
import $script from 'scriptjs';

export default class TransactionWrapper extends Component {
	constructor(props) {
		super();

		let transaction = props.transactionInfo;

		this.state = {
			amount: transaction.amount || 5
		}
		this.updateAmount = this.updateAmount.bind(this);
		this.sendPayment = this.sendPayment.bind(this);
	}

	sendPayment() {
		let btn = this.refs.button;

		Meteor.call('transactionCompleted', this.props.transactionInfo, this.state.amount);

		$script('https://www.simplify.com/commerce/simplify.pay.js', function () {
			btn.click();
		});
	}

	updateAmount(e) {
		let amount = e.target.value;
		let siblings = e.target.parentNode.getElementsByClassName('c-form__button--value');

		for (let i = 0; i < siblings.length; i++) {
			siblings[i].classList.remove('is-active');
		}

		e.target.classList.add('is-active');

		this.setState({ amount });
		this.forceUpdate();
	}

	render() {
		let transaction = this.props.transactionInfo;

		if (transaction) {

			return (
				<section id="transaction">
					<form className="c-form" id="payment-form">
						<div className="c-form__header">
							<h2 className="c-form__title">{ transaction.name }</h2>
						</div>

						<div className="c-form__body">
							<h3 className="c-form__desc">Escolha a quantia que deseja fazer a doação, abaixo</h3>
							<div className="c-form__group">
								<label className="c-form__label">Valor do patrocínio</label>
								<button type="button" className="c-form__button--value" value="5" onClick={ this.updateAmount }>R$ 5</button>
								<button type="button" className="c-form__button--value" value="10" onClick={ this.updateAmount }>R$ 10</button>
								<button type="button" className="c-form__button--value" value="15" onClick={ this.updateAmount }>R$ 15</button>
								<button type="button" className="c-form__button--value" value="20" onClick={ this.updateAmount }>R$ 20</button>
								<button type="button" className="c-form__button--value" value="25" onClick={ this.updateAmount }>R$ 25</button>
							</div>
							<h3 className="c-form__desc">
								Você estará patrocinando <b>{transaction.name}</b> com {this.state.amount} reais
							</h3>
						</div>
						<div className="c-form__footer">
							<button
								onClick={this.sendPayment}
								className="c-form__button"
								data-sc-key="sbpb_ZDg2NWM5MGItMzFkZi00MDI4LTlmM2UtYWFkMzkwNzYxYjJi"
								data-name={transaction.name}
								data-description={'#' + transaction.number}
								data-reference={'#' + transaction.number}
								data-amount={this.state.amount * 100}
								data-color="#12B830"
								data-currency="USD"
								data-receipt="true"
								data-masterpass="true"
								type="button"
								ref="button"
							>
								Finalizar
							</button>
						</div>
						<LoginForm/>
					</form>
				</section>
			)
		} else {
			return (
				<section id="transaction" className="m-centered">
					<p>Para prosseguir com seu patrocínio, entre com seu Facebook.</p>
					<LoginForm/>
				</section>
			)
		}
	}
};
