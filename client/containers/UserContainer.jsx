import { composeWithTracker } from 'react-komposer';
import Profile from '/client/components/user/Profile.jsx';

function composer(props, onData) {
	if (Meteor.user() !== null) {
		const handle = Meteor.subscribe('userInfo');
		
		if (handle.ready()) {
			const userInfo = Meteor.users.findOne().profile;
			onData(null, { userInfo });
		}
		
	} else {
		FlowRouter.go('home');
	}
 
}

export default composeWithTracker(composer)(Profile);