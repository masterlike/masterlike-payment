import { composeWithTracker } from 'react-komposer';
import Home from '/client/components/home/Home.jsx';

function composer(props, onData) {
	if (Meteor.user() !== null) {
		const handle = Meteor.subscribe('userInfo');
		
		if (handle.ready()) {
			const userInfo = Meteor.users.findOne().profile;
			onData(null, { userInfo });
		}
	}

	onData(null, { });
}

export default composeWithTracker(composer)(Home);