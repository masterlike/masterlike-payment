import { composeWithTracker } from 'react-komposer';
import TransactionWrapper from '/client/components/transaction/TransactionWrapper.jsx';

function composer(props, onData) {
	Meteor.call('checkTransaction', props.id, function (err, data) {
		let transactionInfo = false;

		if (!err && typeof data === 'string') {
			FlowRouter.go(data);
		} else if (!err && data) {
			Meteor.call('getTransactionInfo', props.id, function (err, data) {
				transactionInfo = data;
				onData(null, { transactionInfo });
			});
		} else {
			onData(null, { transactionInfo });
		}
	});

}

export default composeWithTracker(composer)(TransactionWrapper);
