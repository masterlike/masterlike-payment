import React from 'react';

// Creting router Layout
export const MainLayout = ({navbar, content, footer}) => (
	<div className="masterlike-app">
		{navbar}
		<main className="content">
			{content}
		</main>
		{footer}
	</div>
);