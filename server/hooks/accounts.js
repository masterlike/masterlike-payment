Accounts.onCreateUser(function (options, user) {
	if (typeof options.profile !== 'undefined') {
		user.profile = options.profile;

		if (typeof user.services.facebook !== 'undefined') {
			user.profile.email = user.services.facebook.email;
			user.profile.first_name = user.services.facebook.first_name;
			user.profile.last_name = user.services.facebook.last_name;
			user.profile.url = user.services.facebook.link;
			user.profile.gender = user.services.facebook.gender;

			let result = Meteor.http.get('https://graph.facebook.com/v2.6/me', {
                  params: { access_token: user.services.facebook.accessToken }
			});

			user.profile.user_id = JSON.parse(result.content).id;
			user.access_token = user.services.facebook.accessToken;
		}
	}

	user.crawled = false;
	user.pages = [];
	user.default_payment = 0;

	return user;
});