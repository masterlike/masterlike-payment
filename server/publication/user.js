Meteor.publish('userInfo', function (userId) {
	return Meteor.users.find({ _id: userId });
});