Meteor.methods({
	checkTransaction: function (id) {
		let link = 'http://localhost:3000/t/' + id;
		let transaction = Transaction.findOne({ link: link });

		if (typeof transaction === 'undefined')
			return 'home';
		
		if (Meteor.userId() && transaction.payed) {
			return 'complete';
		} else if (Meteor.userId() && !transaction.payed) {
			return true;
		}

		return false;
	},

	getTransactionInfo: function (id) {
		let link = 'http://localhost:3000/t/' + id;
		let transaction = Transaction.findOne({ link: link });
		
		if (typeof transaction === undefined)
			return false;
		
		let page = Page.findOne({ page_id: transaction.page_id });

		return {
			_id: transaction._id,
			amount: '5',
			name: page.name,
			number: id,
			currency: 'USD'
		};
	},

	transactionCompleted: function (transaction, amount) {
		Transaction.update(transaction._id, { $set: {
			payed: true,
			buyer_id: Meteor.user().profile.user_id,
			payment: amount
		} });
	},
});
